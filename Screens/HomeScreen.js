
import { createStackNavigator, createAppContainer } from "react-navigation";
import React, { Component } from "react";
import { View, Text, ImageBackground, Image, TouchableOpacity, StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Feather } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
class Login extends Component {
  onPressRegister = () => {
    this.props.navigation.navigate("Register");
  };


  // async setValue() {
  //     await AsyncStorage.setItem('email1', 'ok1');
  // }
  state = {

    loading: true
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('IDTOKEN');

      if (value !== null) {
        // We have data!!
        //alert(value);
        this.props.navigation.navigate("AllProducts");


      } else {
        this.setState({ loading: false })

      }
    } catch (error) {
      // Error retrieving data
    }
  }
  render() {

    
    return (
      <View
        style={{
          flex: 1,
          background: "#44bcd8",

        }}
      >
        <ImageBackground


        >
          <Image
            style={{
              flex: 1,
              height: 80,
              width: 80,
              left: 10,
              top: 40,
              bottom: 70,
              right: 10
              // height: 550
            }}
            source={require("../assets/crop.png")}>

          </Image>

          {/* brandName part */}
          <Text
            style={{
              // fontSize: hp("11.25%"),
              fontSize: 40,
              fontWeight: "bold",
              paddingTop: 40,
              paddingLeft: 80,
              textShadowColor: 'rgba(255, 255, 255, 1)',
              textShadowOffset: { width: 1, height: 1 },
              elevation: 10,
              fontWeight: 'bold',
              textShadowOffset: { width: 1, height: 1 },
              textShadowRadius: 2,
              textShadowColor: 'blue'

            }}
          >
            RENTON
          </Text>

          <Text
            style={{
              // fontSize: hp("11.25%"),
              fontSize: 25,
              paddingTop: 40,
              paddingLeft: 70,
              textShadowColor: 'blue',
              color: "#ffff"

            }}
          >
            Let's rent for All
          </Text>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingBottom: hp("5%"),
              paddingHorizontal: hp("2.5%")
            }}
          >
            <TouchableOpacity style={styles.SubmitButtonStyle} onPress={
              () => this.props.navigation.navigate("About")
            }
            >

              <Text style={styles.text1}>Start with phone number/Email
              <FontAwesome name='arrow-circle-right'
                  style={styles.imageview} />

              </Text>
            </TouchableOpacity>
          </View>




        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({


  SubmitButtonStyle: {

    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 30,
    marginRight: 30,

    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#fff',
    top: 30,
    paddingRight: 5,
    backgroundColor: "#ffff",
    paddingLeft: 3,
    position: "absolute",

  },
  text1: {
    fontWeight: "bold",
    paddingRight: 10,
    fontSize: 13,
    textAlign: "center",


  },
  imageview: {
    fontSize: 20,
    color: '#00000',
    alignItems: "center",
    top: 200,
    left: 120,
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',





  }
})

export default Login;
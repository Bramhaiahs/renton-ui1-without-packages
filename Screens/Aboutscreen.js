import React, { useState } from "react";
import Background from './Background'
import {

  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  state,
  ActivityIndicator,
  ImageBackground,
  DeviceEventEmitter,
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { emailValidator, passwordValidator } from "../core/utils";
import { FontAwesome } from '@expo/vector-icons';
import axios from "axios"
const phoneDetails = (props) => {
  const [indicator, setIndicator] = useState(false);
  const [email, setEmail] = useState({ value: "", error: "" });

  const RegisterAPI = () => {
    //const emailError = email.value;
    const emailError = emailValidator(email.value);
    if (emailError) {
      var number = email.value
      if (number === '' || number.length < 10 || number.length > 10) {
        alert("please enter the 10 digit mobile number")
      }
      else {
        if (isNaN(number)) {
          alert("please enter a valid mobile number")


        }
        else {
          let data = {
            number: number
          }
          //alert('OTP sent ')
          axios.post("http://localhost:3000/customer", data).then(resp => {
            if (resp.data === "passwordpage") {
              props.navigation.navigate("LoginScreen", { email: email.value })

            }
            // let data = []
            // alert(resp)
            let data1 = []
            //alert(resp.data);
            var dataconvert = '';
            dataconvert = JSON.stringify(resp.data);

            // alert(dataconvert);


            data1 = JSON.parse(dataconvert);
            // alert(data1.OTP)
            // alert(data1.OTP)

            var a = data1[0].OTP.toString();
            var p = data1[0].phonenumber.toString();

            //  alert(a)
            props.navigation.navigate("OTP", { otp: a, phonenumber: p })
          })
            .catch(err => {
              console.log(err);
            })
        }
      }

    } else {
      let data = { email: email.value };

      //alert(data.email)
      axios.post("http://localhost:3000/Registeremail", data).then(resp => {
        alert(resp.data)
        if (resp.data === "OTPsent") {
          props.navigation.navigate("OTP", { email: email.value })

        } else {
          props.navigation.navigate("LoginScreen", { email: email.value })

        }
      })
    }
  }

  return (
    <View
      style={{
        flex: 1,
        background: "#44bcd8",

      }}
    >

      <ImageBackground


      >
        <Image
          style={{
            flex: 1,
            height: 80,
            width: 80,
            left: 10,
            top: 40,
            bottom: 70,
            right: 10
            // height: 550
          }}
          source={require("../assets/crop.png")}>

        </Image>

        <Text style={styles.EnterText}>Enter Mobile Number or Email</Text>
        <TextInput
          // onPress={() => this.props.navigation.navigate("Phone")}
          placeholder="Gmail/Phone Number"
          underlineColorAndroid="transparent"
          style={styles.TextInputStyleClass1}
          onChangeText={(text) => setEmail({ value: text, error: "" })}
        >

        </TextInput>

        <View style={styles.TextInputStyleClass2}>
          <TouchableOpacity
            onPress={RegisterAPI}
          //onPress={()=>props.navigation.navigate("OTP")}
          >
            <Text style={styles.buttonText}>Verify</Text>
          </TouchableOpacity>
        </View>
        <ActivityIndicator size="large" color="orange" animating={indicator} />
      </ImageBackground>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  TextInputStyleClass1: {
    textAlign: "center",
    height: 50,
    borderWidth: 2,
    borderColor: "#05075d",
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    top: 300,
    margin: 50,

  },
  EnterText: {
    top: 10,
    marginLeft: 70,
    fontSize: 20,
    color: "#0000",
    right: 20,
  },
  TextInputStyleClass2: {
    textAlign: "center",
    height: 30,
    borderWidth: 2,
    borderColor: "#05075d",
    borderRadius: 15,
    backgroundColor: "#05075d",
    margin: 130,
    width: 200,
    backgroundColor: "#05075d",
    right: 45,
    top: -100,
    bottom: 70

  },
  buttonText: {
    textAlign: "center",
    fontSize: 17,
    color: "#FFFFFF",
    bottom: 30
  },
  imageview: {
    fontSize: 20,
    color: '#00000',
    alignItems: "center",
    top: 200,
    left: 120,
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'




  }
});
export default phoneDetails;

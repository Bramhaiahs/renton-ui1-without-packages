import React, { memo, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Background from "../Screens/Background";
import Logo from "../Screens/Logo";
import Header from "../Screens/Header";
import Button from "../Screens/Button";
import AsyncStorage from '@react-native-community/async-storage';
import TextInput from "../Screens/TextInput";
import BackButton from "../Screens/BackButton";
import { theme } from "../core/theme";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axios from "axios";
import {
  emailValidator,
  passwordValidator,
  nameValidator,
} from "../core/utils"; const RegisterScreen = ({ navigation }) => {
  const [name, setName] = useState({ value: "", error: "" });
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const _onSignUpPressed = async () => {
    //  const { navigation } = this.props;
    const email1 = navigation.getParam('email', '')
    //alert(email1)
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email1);
    const passwordError = passwordValidator(password.value);

    // if (emailError || passwordError || nameError) {

    //   setName({ ...name, error: nameError });
    //   setEmail({ ...email, error: emailError });
    //   setPassword({ ...password, error: passwordError });
    //   return;
    // } else
    {

      let data = {
        username: name.value,
        email: email1,
        password: password.value
      }


      // alert('email: ' + data.number + ' password: ' + pass)
      // alert(data.password)
      axios.post("http://localhost:3000/CreateAccount", data).then(async resp => {
        //alert(resp.data.id)

        await AsyncStorage.setItem('IDTOKEN', resp.data.id);
        await AsyncStorage.setItem('username', resp.data.username);

        navigation.navigate("location")
      })
        .catch(err => {
          console.log(err);
        })




    }

    //navigation.navigate("Dashboard");
  };

  return (
    <View style={styles.container}>
      <Background>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />
        <Logo />

        <Header color="#ffff">Create Account</Header>

        <TextInput
          style={styles.textinput1}
          label="Name"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({ value: text, error: "" })}
          error={!!name.error}
          errorText={name.error}
        />



        <TextInput
          style={styles.textinput3}
          label=" Coform Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
          left1="false"
        />

        <Button
          mode="contained"
          onPress={_onSignUpPressed}
          style={styles.button}
        >
          SUBMIT
        </Button>

        {/* <View style={styles.row}>
        <Text style={styles.label}>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate("LoginScreen")}>
          <Text style={styles.link}>Login1</Text>
        </TouchableOpacity>
      </View> */}
      </Background>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    color: "#ffff",
  },
  container: {
    flex: 1,
    backgroundColor: "#44bcd8",

    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 360,
    height: 600,
  },
  button: {
    marginTop: 24,
    bottom: 10,
    backgroundColor: "#05075d"
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  textinput1: {
    height: 45,
  },
  textinput2: {
    height: 45,
  },
  textinput3: {
    height: 45,
  },
  text6: {
    color: "white",

  },
});
export default memo(RegisterScreen);



import axios from "axios"
import React, { Component } from "react";
import Background from './Background'
import {
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    Image
} from "react-native";

import OTPTextView from "react-native-otp-textinput";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: "#44bcd8"
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        fontSize: 25,
        fontWeight: "500",
        textAlign: "center",
        color: "#ffff",
        marginBottom: 20,
        top: 190,
        position: "absolute",
        left: 80,


    },
    imageview: {
        fontSize: 20,
        color: '#00000',
        alignItems: "center",
        top: 200,
        left: 120,
        paddingLeft: 10,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'




    },
    textInputContainer: {
        marginBottom: 10,
        top: 270,
        position: "absolute",
        left: 60,
        tintColor: "#ff0000",
        offTintColor: "#ff0000"


    },
    roundedTextInput: {
        borderWidth: 4,
        borderRadius: 20,
        tintColor: "#ff0000",
        offTintColor: "#ff0000"


    },
    buttonWrapper: {

        flexDirection: "row",
        justifyContent: "space-around",
        marginBottom: 20,
        width: "60%",

    },
    textInput: {
        height: 40,
        width: "80%",
        borderColor: "#000",
        borderWidth: 1,
        padding: 10,
        fontSize: 16,
        letterSpacing: 5,
        marginBottom: 10,
        textAlign: "center",
    },
    button: {
        height: 30,
        backgroundColor: "#05075d",
        width: 120,
        borderRadius: 12,
        top: 360,
        left: 120,
        position: "absolute",
    },
    buttonText: {
        alignContent: "center",
        textAlign: "center",
        fontSize: 20,
        color: "#FFFFFF",
    },

    buttonStyle: {
        marginHorizontal: 20,
    },
    container4: {
        bottom: 470,
        height: 58,
        width: 68,
        alignItems: 'center',
        backgroundColor: "#fff",
        position: 'absolute',
        border: '2px solid',
        borderRadius: '40%',

    },
    instructions1: {
        fontWeight: "500",
        textAlign: "center",
        color: "black",
        marginBottom: 20,
        top: 230,
        position: "absolute",
        left: 60,

    }
});

export default class otpScreen extends Component {
    state = {
        otpInput: "",
        inputText: "",
        defaultValue: "",
    };
    OTPverification = () => {
        const { otpInput } = this.state;
        const { navigation } = this.props;
        var email1 = navigation.getParam('email', '')
        // var otp1 = navigation.getParam('otp', '')
        var phonenumber = navigation.getParam('phonenumber', '')

        var phonenumber1 = navigation.getParam('phonenumber', '')
        var data;

        if (phonenumber1 != '') {
            //  alert(phonenumber1)
            data = {
                otp: otpInput,
                phonenumber: phonenumber1,
            }

        }
        else {
            data = {
                otp: otpInput,
                email: email1,
            }
        } //alert("ok")

        axios.post("http://localhost:3000/OTPVALIDATION", data).then(resp => {
            // alert(resp.data);

            if (phonenumber === '') {
                this.props.navigation.navigate("RegisterScreen", { email: email1 })


            } else {
                if (resp.data.result === 'success') {
                    this.props.navigation.navigate("RegisterScreen", { email: phonenumber })
                } else {
                    alert("Incorrect OTP")
                }
            }
        })
    }

    alertText = () => {
        const { otpInput = "" } = this.state;
        if (otpInput) {
            Alert.alert(otpInput);
        }
    };

    clear = () => {
        this.input1.clear();
    };

    updateOtpText = () => {
        // will automatically trigger handleOnTextChange callback passed
        this.input1.setValue(this.state.inputText);
    };

    render() {
        const { otpInput } = this.state;
        const { navigation } = this.props;
        // var otp = navigation.getParam('otp', '')
        // this.state.otpInput = otp;
        return (
            <View style={styles.container}>
                <Background>




                    <View style={{
                        flex: 1,
                        background: "#44bcd8",

                    }}>
                        <ImageBackground>
                            <Image
                                style={{
                                    flex: 1,
                                    height: 80,
                                    width: 80,

                                    top: 20,
                                    bottom: 100,
                                    right: 130,


                                    // height: 550
                                }}
                                source={require("../assets/crop.png")}>

                            </Image>
                        </ImageBackground>

                    </View>


                    <View style={styles.container4}>
                        <AntDesign name="message1" size={44} backgroundColor="#05075d" />
                    </View>

                    <Text style={styles.instructions}>Verify OTP number</Text>

                    <Text style={styles.instructions1}>check your email/phone number for OTP</Text>

                    {/* <View style={styles.container2}>
                    <MaterialCommunityIcons name="onepassword" size={24} color="black" />
                </View> */}


                    <OTPTextView
                        ref={(e) => (this.input1 = e)}

                        focusStyles={[{ bordercolor: 'red' }]}
                        containerStyle={styles.textInputContainer}
                        handleTextChange={(text) => this.setState({ otpInput: text })}
                        inputCount={4}
                        keyboardType="numeric"
                        //defaultValue={this.state.otpInput}
                        textInputStyle={[styles.roundedTextInput, { borderRadius: 100, tintColor: "#ff0000", offTintColor: "#ff0000" }]}
                    />
                    {/* <TextInput
          maxLength={4}
          onChangeText={(e) => this.setState({inputText: e})}
          style={styles.textInput}
        /> */}
                    <View style={styles.button}>
                        <TouchableOpacity
                            onPress={this.OTPverification}
                        //onPress={()=>this.props.navigation.navigate("RegisterScreen")}
                        >
                            <Text style={styles.buttonText}>Submit</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity
            style={styles.buttonStyle}
            title="Update"
            onPress={this.updateOtpText}
          /> */}
                        {/* <TouchableOpacity
            style={styles.button1}
            title="Submit"
            onPress={this.alertText}
          >
          <Text style={styles.buttonText}>Submit</Text>
          </TouchableOpacity> */}
                    </View>
                    {/* <Text style={styles.instructions}>Customizations</Text> */}
                    {/* <OTPTextView
          handleTextChange={(e) => {}}
          containerStyle={styles.textInputContainer}
          textInputStyle={styles.roundedTextInput}
          inputCount={5}
          inputCellLength={2}
        /> */}
                    {/* <OTPTextView
          handleTextChange={(e) => {}}
          containerStyle={styles.textInputContainer}
          textInputStyle={styles.roundedTextInput}
          defaultValue="1234"
        /> */}
                    {/* <OTPTextView
          handleTextChange={(e) => {}}
          containerStyle={styles.textInputContainer}
          textInputStyle={[styles.roundedTextInput, {borderRadius: 100}]}
          tintColor="#000"
        /> */}

                </Background>
            </View>
        );
    }
}

import React, { memo, useState, Component } from "react";
import { TouchableOpacity, StyleSheet, Text, View, Image, ImageBackground } from "react-native";
import Background from "../Screens/Background";
import Logo from "../Screens/Logo";
import Header from "../Screens/Header";
import Button from "../Screens/Button";
import TextInput from "../Screens/TextInput";
import BackButton from "../Screens/BackButton";
import { theme } from "../core/theme";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axios from "axios"
import { emailValidator, passwordValidator } from "../core/utils";
import { Entypo } from '@expo/vector-icons';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const _onLoginPressed = () => {
    const email1 = navigation.getParam('email', '')
    const emailError = emailValidator(email1);
    const passwordError = passwordValidator(password.value);
    // if (emailError || passwordError) {
    //   alert("please enter a valid email")
    //   setEmail({ ...email, error: emailError });
    //   setPassword({ ...password, error: passwordError })
    // }
    // else 
    {
      let data = {
        email: email1,
        password: password.value,
      }
      // alert('email: ' + data.number + ' password: ' + pass)
      axios.post("http://localhost:3000/login", data).then(resp => {
        // alert(resp.data)
        if (resp.data === "Admin Login") {
          navigation.navigate('Productadd')

        } else {
          if (resp.data === "Invalid password") {
            alert('Invalid password')
          } else {
            navigation.navigate("AllProducts")
          }
        }
      })
        .catch(err => {
          console.log(err);
        })
    }
  }
  //navigation.navigate("Dashboard");
  return (
    <View style={styles.container}>
      <Background>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />
        <ImageBackground>
          <Image
            style={{
              flex: 1,
              height: 80,
              width: 80,


              bottom: 90,
              right: 20
              // height: 550
            }}
            source={require("../assets/crop.png")}></Image>
          <View style={styles.icon}>
            <Entypo name="lock" size={44} color="darkblue" />
          </View>

          <View style={styles.passwordview}>
            <Text style={styles.password}>Enter password</Text>
          </View>



          <Header></Header>

          {/* <TextInput
          style={styles.inputtext}
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({ value: text, error: "" })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        /> */}

          <TextInput
            label="Password"
            returnKeyType="done"
            value={password.value}
            onChangeText={(text) => setPassword({ value: text, error: "" })}
            error={!!password.error}
            errorText={password.error}
            secureTextEntry

          />

          <View style={styles.forgotPassword}>
            <TouchableOpacity
              onPress={() => navigation.navigate("About")}
            >
              <Text style={styles.label}>Forgot your password?</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={styles.loginbutton} onPress={_onLoginPressed}>
            <View style={styles.loginText1}>
              <Text style={styles.loginText}>Login</Text>
            </View>
          </TouchableOpacity>

          {/* <View style={styles.row}>
          <Text style={styles.label}>Don’t have an account? </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("RegisterScreen")}
          >
            <Text style={styles.link}>Sign up</Text>
          </TouchableOpacity>
        </View> */}

        </ImageBackground>
      </Background>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 360,
    height: 600,
    backgroundColor: "#44bcd8"
  },
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  inputtext: {
    borderRadius: 30,
    marginBottom: 1,
  },
  passwordview: {
    bottom: -25,
    left: 40,
    align: "center",

  },
  password: {
    font: "bold",
    fontSize: 24,
    color: "white",
    fontWeight: "bold",
  },
  icon: {
    left: 90,
    bottom: 250,
    height: 58,
    width: 68,
    alignItems: 'center',
    backgroundColor: "#fff",
    position: 'absolute',
    border: '2px solid',
    borderRadius: '50%',
  },
  loginbutton: {
    borderRadius: 20,
    backgroundColor: "white",
    height: 40,
    alignItems: "center",
    border: '2px solid',


  },
  loginText: {
    fontWeight: "bold",
    fontSize: 21,
    alignItems: "center",
    alignContent: "center",
    color: "darkblue",



  },
  loginText1: {
    top: 3
  }
});

export default memo(LoginScreen);
